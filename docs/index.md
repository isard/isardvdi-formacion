# Formación IsardVDI

En este curso trabajaremos diferentes tecnologías que intervienen en IsardVDI.

Es necesario tener nociones previas de:

- línea de órdenes de Linux
- Redes y dispositivos de red
- Base de programación

Utilizaremos escritorios virtuales basados en Ubuntu 20.04

