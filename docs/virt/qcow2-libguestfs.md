# libguestfs

Existen una serie de utilidades que nos permitirán realizar operaciones muy interesantes sobre discos, normalmente sin que su máquina virtual esté iniciada simultáneamente [https://www.libguestfs.org/](https://www.libguestfs.org/).

Comentaremos algunas que pueden ser útiles.

## virt-filesystems & guestfish & virt-resize

Con estas utilidades podremos listar los sistemas de ficheros que haya creados dentro del disco virtual y también redimensionar un disco y, a diferéncia de lo comentado en el apartado anterior, con el *virt-resize* podremos también redimensionar a la vez las particiones y sistemas de ficheros que haya dentro del disco ya creadas.

    virt-filesystems virt-filesystems -a disco.qcow2 
    /dev/sda1
    /dev/sda15

Sabiendo eso podemos pasar a crear un nuevo disco de mayor tamaño y usar *virt-resize* para copiar los datos en este nuevo disco y incrementar la partición.

    qemu-img create -f qcow2 out.qcow2 20G
    virt-resize --expand /dev/sda1 disco.qcow2 out.qcow2

También podemos usar el interactivo de órdenes *guestfish* para investigar un poco más el disco:

    guestfish -a disco.qcow2

## virt-customize

Nos permitirá aplicar personalizaciones a sistemas operativos, la mayoria para Linux.

Podemos descargar la imágen preparada de Ubuntu 18.04 de cloud y cambiar el password de root antes de crear una máquina a partir de ese disco:

    - wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img
    - sudo virt-customize -a bionic-server-cloudimg-amd64.img --root-password password:ubuntu

## virt-install & virt-builder

Estas utilidades nos permitirán crear una máquina virtual con todo su hardware a partir de un disco (*virt-install*) o incluso instalar desde cero una distribución en una máquina virtual (*virt-builder*)

Esto creará una nueva máquina virtual a partir de *disco.qcow2*:

    virt-install \
    --name UbuntuBionic \
    --memory 2048 \
    --vcpus 2 \
    --disk bionic-server-cloudimg-amd64.img \
    --import \
    --os-variant ubuntu18.04

Para crear una máquina virtual a partir de discos precreados podemos escojer entre los que hay disponibles en [https://builder.libguestfs.org/](https://builder.libguestfs.org/) o con la órden *virt-builder --list* y ver notas de instalación con *virt-builder --notes ubuntu-20.04*.

Esto creará una nueva máquina virtual a partir de la base que descargará del repositorio de virt-builder anterior, con disco en formato qcow2 de 10G, clave de root T3ST y añadirá el usuario isard con password isard

    virt-builder ubuntu-20.04 \
    --hostname vm-ubuntu \
    --format qcow2 \
    --root-password password:T3ST \
    --firstboot-command 'useradd -m -p "" isard ; chage -d 0 isard'

## virt-sparsify

Esta utilidad permite hacer lo que tradicionalmente conocemos como *defragmentar* el disco. Esto es especialmente útil con discos de sistemas operativos que se han estado usando para instalar y desinstalar aplicaciones. Los datos que vamos añadiendo en los discos incrementales (no tiene sentido en discos que no sean incrementales) una vez borrados no decrementan el espacio usado por el disco virtual.

Para poder recuperar los espacios sin datos del disco y reducir el tamaño que nos está ocupando en disco haremos:

    virt-sparsify --inplace disco.qcow2

## virt-copy-in & virt-copy-out

Estas órdenes nos permitirán copiar en un directorio dentro del disco virtual datos, o viceversa.

    virt-copy-in -a disco.qcow2 test.txt /etc

Para comprobar que lo ha metido dentro:

    virt-cat -a disco.qcow2 /etc/test.txt

## virt-inspector

Nos permite extraer información del sistema operativo y aplicaciones que pueda encontrar en las particiones dentro del disco virtual, en formato xml:

    virt-inspector disco.qcow2

## guestfish

Esta utilidad en realidad inicia un mini sistema operativo con sus propias órdenes que nos permitirá actuar sobre un disco

    guestfish -a disco.qcow2

La secuencia de órdenes empieza con un *run* y acaba con un *exit*:

    run
    list-filesystems
    mount /dev/vda1 /
    ls /
    umount

Podemos mirar en la ayuda las órdenes que hay [https://www.libguestfs.org/guestfish.1.html](https://www.libguestfs.org/guestfish.1.html)

En esta página encontraremos muchos ejemplos de estas órdenes [https://ostechnix.com/access-and-modify-virtual-machine-disk-images-with-libguestfs/](https://ostechnix.com/access-and-modify-virtual-machine-disk-images-with-libguestfs/)