# Libvirt

Se trata de una libreria en diferentes lenguajes (Python, GoLang,..) que hace de interfaz con la virtualización de máquinas y redes virtuales. tine la posibilidad de comunicarse, no sólo con KVM/qemu sinó con otros virtualizadores.

## Libvirt en Python

Referéncia: [https://libvirt.org/python.html](https://libvirt.org/python.html)

Mediante esta libreria podremos crear una conexión y realizar las operaciones y configuraciones de las máquinas virtuales.

Aquí tenemos un ejemplo de su uso: [https://gist.github.com/archerslaw/9801571](https://gist.github.com/archerslaw/9801571)

Que dejamos aquí convertido a python3:

```
import libvirt
conn = libvirt.open('qemu:///system')
for id in conn.listDomainsID():
    dom = conn.lookupByID(id)
    print("Dom %s  State %s" % ( dom.name(), dom.info()[0] ))
    dom.suspend()
    print("Dom %s  State %s (after suspend)" % ( dom.name(), dom.info()[0] ))
    dom.resume()
    print("Dom %s  State %s (after resume)" % ( dom.name(), dom.info()[0] ))
    dom.destroy()

# The libvirt API concepts.
# http://libvirt.org/api.html

# == Sample programs (Python) ==

# 1. Define a domain
import libvirt  
conf_file = 'dom.xml'
file = open(conf_file)
xml = file.read()
conn = libvirt.open('qemu:///')
conn.defineXML(xml)

# 2. Start a domain & display information & destroy
import libvirt
vmname = 'dom1'
conn = libvirt.open('qemu:///')
dom = conn.lookupByName(vmname)
#create domain
dom.create()
#print domain information
print(dom.info())
dom.destroy  

# All libvirt commands could be found on virsh which uses Libvirt API.

# == Libvirt python API binding ==
# Libvirt is an open source API, daemon and management tool for managing platform virtualization. It also provide various language API binding and this post is about to do common stuff using python API binding.

# Before you start make sure required packages/tools installed in your system.
# if you are using fedora then execute "yum install @virtualization"

# API Concepts: http://libvirt.org/api.html -> Functions and Naming Conventions

import libvirt

# Get a connection to Hypervisor (optional URI).
# Return virConnect object if successfull otherwise NULL
# More: http://libvirt.org/html/libvirt-libvirt.html#virConnectOpen
# Check openAuth and openReadOnly methods also.
URI='qemu:///'
virConnect_obj = libvirt.open(URI)

# List of defined domains which are not active on hypervisor.
# Return a List of defined domains or -1 in case of error
# More: http://libvirt.org/html/libvirt-libvirt.html#virConnectListDefinedDomains

inactive_defined_domain = virConnect_obj.listDefinedDomains()

# List of domains which are active on hypervisor
# Return a List of active domains ID or -1 in case of error
# More: http://libvirt.org/html/libvirt-libvirt.html#virConnectListDomains

active_domain = virConnect_obj.listDomains()

# Define a VM using xml description of domain. Previous definition will be overwritten if already exist.
# Return virDomain obj in case of success otherwise NULL.
# More: http://libvirt.org/html/libvirt-libvirt.html#virDomainDefineXML

xml_description = "<String containing xml desc>"
virDomain_obj = virConnect_obj.defineXML(xml_description)

# Create a VM, here it will also start the VM.
# Return 0 in case of success, -1 if error
# More: http://libvirt.org/html/libvirt-libvirt.html#virDomainCreate
# Check out virDomainCreateWithFiles, virDomainCreateWithFlags .. etc. Use whatever best suit.

status = virDomain_obj.create()

# Delete a VM
# Return 0 in case of success and -1 if error
# More:  http://libvirt.org/html/libvirt-libvirt.html#virDomainDestroy

status = virDomain_obj.destroy()

# There are lot of API calls which you can use to automate stuff as per requirement -> http://libvirt.org/html/libvirt-libvirt.html

# Hope it will help to get start with libvirt ......
```