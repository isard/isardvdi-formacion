# qemu-img

Esta es la principal utilidad para gestionar almacenamiento virtual. El almacenamiento virtual habitualmente serán ficheros que las máquinas virtuales verán como discos.

[https://qemu.readthedocs.io/en/latest/tools/qemu-img.html](https://qemu.readthedocs.io/en/latest/tools/qemu-img.html)

El principal uso que hacemos del qemu-img es para crear discos, con el formato virtual qcow2, aunque hay más formatos [https://qemu.readthedocs.io/en/latest/tools/qemu-img.html#notes](https://qemu.readthedocs.io/en/latest/tools/qemu-img.html#notes).

Hay más información y ejemplos en:

- [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/chap-using_qemu_img](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/chap-using_qemu_img)
- [https://blog.programster.org/qemu-img-cheatsheet](https://blog.programster.org/qemu-img-cheatsheet)


NOTA: Para la mayoría de estas operaciones no debe haber ninguna máquina virtual iniciada que esté usando el disco sobre el que estamos actuando, los datos se podrían corromper.

## Creación de discos

### No incrementales

Ocuparán todo el espacio de inicio:

    qemu-img create disco_no_incremental.qcow2 1G

Para ver información detallada de disco:

    qemu-img info disco_no_incremental.qcow2

## Incrementales

Prácticamente no ocupan nada de inicio y se irán incrementando cuando añadamos datos:

    qemu-img create -f qcow2 disco_incremental.qcow2 1G


## Disco derivado de otro disco

Podemos dejar un disco como estaba, con sus datos, y crear un nuevo disco que será una nueva capa sobre su anterior (backing-chain):

    qemu-img create -F qcow2 -b disco_incremental.qcow2 -f qcow2 disco_desde_incremental.qcow2

Los cambios que hagamos en este nuevo disco no se verán reflejados en su disco 'padre' (backing-chain). A menos que queramos explícitamente 'subirle' esos cambios con un commit.

## Subir cambios de un disco a su padre

Esta operación tiene el riesgo de que, en la estructura de árbol que podemos crear con discos derivados, si cambiamos el padre de esos discos derivados todas las derivadas quedarán inutilizables (sus datos base han cambiado y ya no concuerdan con las del incremental).

[https://wiki.qemu.org/Features/Snapshots#Block_Commit_[proposal,_preliminary]](https://wiki.qemu.org/Features/Snapshots#Block_Commit_[proposal,_preliminary])

    qemu-img commit disco_desde_incremental.qcow2

Esto habrá subido los cambios de **disco_desde_incremental.qcow2** a su padre **disco_incremental.qcow2**.

*IMPORTANTE*: cualquier otro disco que se hubiera derivado de *disco_incremental.qcow2* deja de ser válido a partir de ahora. Excepto el que hemos usado para subir sus datos a su padre, porque estos están 'sincronizadas' sus capas.

## Crear un nuevo disco a partir de toda la cadena de discos

Cuando queramos tener un disco con todos los datos de su cadena podemos usar la función convert:

    qemu-img conver -f qcow2 -O qcow2 disco_desde_incremental.qcow2 nuevo_disco.qcow2

Esta operación no modificará nada de los discos existentes. Creará el nuevo *disco.qcow2* con todos los datos que lo conformaban en su cadena de discos.

## Cambiar la ruta de parent de un disco

Si en el ejemplo anterior moviéramos a otro directorio el *disco_incremental.qcow2*, dejaría de ser funcional el disco *disco_desde_incremental.qcow2* que dependía de ese.

En ese caso podemos simplemente cambiar la ruta del padre (backing-chain) del disco para que lo vuelva a encontrar:

    qemu-img rebase -f qcow2 -u -b /nuevo_directorio/disco_incremental.qcow2 disco_desde_incremental.qcow2

Con la opción '-u' (unsafe) hemos indicado que no haga comprobaciones internas de integridad, sólo que proceda a cambiar el parámetro de 'backing-chain' del disco. Si no usamos la opción '-u' entonces el proceso es mucho más lento porque hará comprobación de integridad de todos los datos entre el disco derivado y el padre.


## Redimensionar un disco

Redimensionar un disco es como incrementar su tamaño físico, pero no tocaremos nada de sus particiones o volúmenes lógicos que ya pueda tener creados.

    qemu-img resize disco_desde_incremental.qcow2 +1G

Ahora tendrá 1G más que podremos comprobar con *qemu-img info disco_desde_incremental.qcow2*. Su imágen padre no ha sido alterada, y sigue teniendo el mismo espacio total de disco.

## Snapshots

Los *snapshots* son instantaneas del estado del disco, que se pueden hacer en caliente (live, es decir con la máquina virtual asociada iniciada). A diferencia de los discos incrementales, en dónde creamos un nuevo disco a partir de otro, en los *snapshots* creamos una nueva capa dentro del mismo disco, dejando 'congeladas' las anteriores. Esta nueva capa irá añadiendo los nuevos datos que vayamos añadiendo al disco.

    qemu-img snapshot -c "Aplicaciones instaladas" disco_desde_incremental.qcow2
    qemu-img snapshot -l disco_desde_incremental.qcow2

Para volver la imágen a un snapshot anterior:

    qemu-img snapshot -a "Aplicaciones instaladas" disco_desde_incremental.qcow2

Para eliminar una capa de *snapshot*:

    qemu-img snapshot -d "Aplicaciones instaladas" disco_desde_incremental.qcow2

## Convertir formatos de imágenes de disco

Podemos usar la función *convert** para convertir entre múltiples formatos de disco [https://docs.openstack.org/image-guide/convert-images.html](https://docs.openstack.org/image-guide/convert-images.html)

    qemu-img convert -f vmdk -O qcow2 disco_vbox.vmdk disco_qcow.qcow2

Estas operaciones suelen requerir de mucho consumo de disco y de cpu. Podemos probar con la imágen de cloud `wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img`.

## Otras opciones

Entre sus opciones tenemos estas interesantes que podemos encontrar en los enlaces referenciados:

- check
- dd
- map
- measure
