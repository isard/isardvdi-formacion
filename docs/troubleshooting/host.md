# Server troubleshooting

Para analizar los problemas y alertas de un servidor Linux básicamente deberemos fijarnos en los logs. 

Tradicionalmente se han gestionado con `syslog` en ficheros en la carpeta `/var/log` [https://www.loggly.com/ultimate-guide/linux-logging-basics/](https://www.loggly.com/ultimate-guide/linux-logging-basics/) aunque actualmente las distribuciones de Linux que vienen con `systemd` lo complementan (o incluso sustituyen) por `journald` [https://www.loggly.com/ultimate-guide/using-journalctl/](https://www.loggly.com/ultimate-guide/using-journalctl/).

## logs
### syslog

Los logs de sistema los veremos en ficheros en el mismo `/var/log` [https://thehackerway.com/2021/06/21/15-ficheros-de-log-interesantes-en-sistemas-linux/](https://thehackerway.com/2021/06/21/15-ficheros-de-log-interesantes-en-sistemas-linux/)

Encontraremos en la raiz los principales de sistema (kern, messages, syslog, dmesg, ...) y también algunos otros de servicios instalados. Algunos servicios incluso hacen dentro su propia carpeta para sus ficheros.

La utilidad `dmesg` nos permitirá ver los logs de kernel y de sistema desde que arrancó el sistema (en realidad analiza los datos del fichero `/var/log/kern.log`) pero nos es más útil porque podemos aplicar parámetros (siempre tendremos la información de los comandos en su `man dmesg`):

    dmesg -wHT

    -w: esperar nuevos mensajes
    -H: formato de unidades 'humano'
    -T: formato de tiempo 'humano'

Para ver los logs de un servicio en concreto como un servidor web nginx encontraremos su fichero o carpeta dentro de /var/log.

Los ficheros comprimidos .gz són generados por la *rotación* que se configura como un *cron* en `/etc/cron.daily/logrotate` y que usa el binario `logrotate` para ir comprimiendo los datos de logs antíguos [https://www.redhat.com/sysadmin/setting-logrotate](https://www.redhat.com/sysadmin/setting-logrotate).

### journald

Como hemos comentado, las distribuciones actuales usan journald, que es un servicio que tendremos ejecutándose [https://learning.lpi.org/es/learning-materials/102-500/108/108.2/108.2_02/](https://learning.lpi.org/es/learning-materials/102-500/108/108.2/108.2_02/):

    systemctl status systemd-journald

Este servicio crea ficheros binarios estructurados e indexados. Para acceder a ellos deberemos usar su herramienta `journalctl`:

    journalctl

Para poder filtrar i movernos por los logs con facilidad, algunas opciones útiles son:

- `journalctl -b -1`: logs de el último (o últimos) inicios de sistema. Con el número podremos indicar qué arranque queremos ver en el historial.
- `journalctl --list-boots`: listar todos los arranques que ha habido.
- `journalctl -k`: logs equivalentes del **dmesg*.
- `journalctl -u nginx`: logs de un servicio en concreto, en este ejemplo del servicio *nginx*.
- `journalctl -u nginx -f`: logs con *follow*, por lo tanto se irán actualizando cuando aparezcan nuevos eventos.

La cantidad de datos que almacena puede ser gigante, así que es recomendable analizar y limitar estos (a la manera del *logrotate* de syslog).

    journalctl --disk-usage

En el fichero `/etc/systemd/journald.conf` podremos configurar el tamaño máximo al que queremos que llegue con diferentes parámetros:

- SystemMaxUse=500M
- SystemKeepFree=4G
- SystemMaxFileSize..
- ...

## Utilidades

Entre las utilidades comunes para analizar problemáticas con el servidor tenemos:

- **top** / **htop**: carga, estado de la memoria, procesos, ... [https://marcosaguilar.es/herramienta-htop-en-linux/(https://marcosaguilar.es/herramienta-htop-en-linux/)]
- **glances**: estado general del sistema y avisos de sobrecarga (I/O wait) [https://glances.readthedocs.io/en/latest/quickstart.html](https://glances.readthedocs.io/en/latest/quickstart.html)
- **netstat** / **ss**: puertos abiertos escuchando [https://www.redhat.com/sysadmin/netstat](https://www.redhat.com/sysadmin/netstat)
- **iftop**: análisis de velocidad de red
- **smartctl**: estado de los discos

Entre las causas comunes de problemas de rendimiento en servidores se encuentra su falta de memoria. con *htop* podemos verificar si el sistema está usando memoria *swap*. En un servidor no es recomendable que haga *paginación* de ram a disco y por lo tanto mejor desactivarla:

    swapoff -a

O para siempre eliminando del `/etc/fstab` el montaje de la partición/fichero de swap.

También podemos jugar a bajar el *swappiness* al mínimo poniendo en el fichero `/etc/sysctl.conf`:

    vm.swappiness=1

Por defecto los Linux suelen tener el swappines a 60 (100 usaría intensivamente la swap y 1 lo evitará prácticamente siempre).



# Docker troubleshooting

Para analizar posibles problemas con sistemas de containers *Docker* siempre empezaremos por analizar el estado del servicio y sus logs si hay problemas:

    systemctl status docker

    journalctl -u docker -r -p err

    -r: orden de tiempo inverso
    -p <level>: nivell de error (err, warning, notice, info, debug)

Teniendo el servicio funcional (active) deberemos analizar qué ha sucedido al arrancar los contenedores mirando su estado en la columna *STATUS*:

    docker compose ps --all

```
NAME                        IMAGE                                                     COMMAND                  SERVICE                     CREATED             STATUS                  PORTS
isard-api                   registry.gitlab.com/isard/isardvdi/api:main               "/bin/sh -c /run.sh"     isard-api                   11 hours ago        Up 11 hours (healthy)   
isard-authentication        registry.gitlab.com/isard/isardvdi/authentication:main    "/run.sh"                isard-authentication        11 hours ago        Up 11 hours    
```

Normalmente debería indicar 'Up' y, si tiene un servicio de comprovación de estado *healthcheck* entonces también nos indicará su estado de *salud*. 

Para añadir una comprobación de estado del contenedor a un docker-compose.yml tendremos que añadir el apartado *healthcheck* [https://docs.docker.com/engine/reference/builder/#healthcheck](https://docs.docker.com/engine/reference/builder/#healthcheck) con un test al que responda el servicio que esté corriendo dentro del contenedor. Por ejemplo:

```
healthcheck:
  test: ["CMD", "curl", "-f", "http://localhost"]
  interval: 1m30s
  timeout: 10s
  retries: 3
  start_period: 40s
```

En el caso que el contenedor esté en estado *Exited* deberemos analizar en sus logs qué ha pasado:

```
docker compose ps --all

NAME                IMAGE                                         COMMAND                  SERVICE             CREATED             STATUS                      PORTS
isard-api           registry.gitlab.com/isard/isardvdi/api:main   "python3 -u /api/sta…"   isard-api           12 hours ago        Exited (0) 16 seconds ago
```

    docker compose logs isard-api

    docker logs isard-api --since 2m

A partir de ahí ya dependerá de cada contenedor lo que haga internamente el análisis y solución posterior.

Para analizar con detalle un contenedor tenemos la opción de hacer una inspección:

    docker inspect isard-api

Todos los parámetros que ha usado docker para crear el contenedor desde la imágen se encuentran en ese comando.

Recordemos que es importante limitar también la cantidad de logs que vamos a guardar de cada contenedor y también indicar su formato si queremos:

```
    logging:
      driver: json-file
      options:
        max-size: 100m
```

Un apartado que suele traer problemáticas con los dockers es cuando queremos jugar con las redes:

    docker network ls

Docker crea por defecto redes bridge (switch) y también de host. Aunque tiene otros tipos que quedan más allá del ámbito de este curso:

- [https://docs.docker.com/network/bridge/](https://docs.docker.com/network/bridge/)
- [https://docs.docker.com/network/host/](https://docs.docker.com/network/host/)
- [https://docs.docker.com/network/ipvlan/](https://docs.docker.com/network/ipvlan/)
- [https://docs.docker.com/network/macvlan/](https://docs.docker.com/network/macvlan/)
- [https://docs.docker.com/network/none/](https://docs.docker.com/network/none/)

Las redes `bridge` són las por defecto en docker y el servicio docker se encarga por defecto de asignar IP y DNS mediante un servicio dnsmasq y reglas *iptables* para permitir *NAT* hacia las interfaces reales del host.

El modo de red host en realidad lo que hace es saltarse el aislamiento por defecto de los contenedores y ese contenedor tendrá acceso directo a la interfaz real del host. Esto puede ser útil si tenemos más de una interfaz en el host y queremos usar una exclusivamente en un contenedor.

Para analizar la red también podremos inspeccionarlas:

    docker network inspect bridge

Las problemáticas muchas veces pueden venir de la resolución de DNS o del uso de un firewall externo. La configuración del comportamiento general de *docker* se puede definir en un fichero en `/etc/docker/daemon.json` [https://gist.github.com/melozo/6de91558242fb8ca4212e4a73fbddde6](https://gist.github.com/melozo/6de91558242fb8ca4212e4a73fbddde6)

Si la problemática fuera con los dns podemos indicar en ese fichero los DNS que deben usar nuestros contenedores:

```
{"dns":["8.8.8.8"]}
```

El otro caso interesante es si queremos limitar desde dónde se accede a nuestros contenedores. Por defecto hemos visto que en la definición de contenedores indicamos los puertos que deben abrir. Docker añade reglas iptables:

    iptables-save | grep DOCKER

Pero no tenemos control desde dónde se podrá acceder a esos puertos. Si queremos tener un *firewall* en el mismo servidor que se ejecutan los contenedores, entonces deberemos configurar el docker para que no abra puertos y hacerlo nosotros con reglas *forward* en nuestro firewall:

```
{ "iptables": false }
```

Por ejemplo, si tenemos un **firewalld** entonces podremos abrir allí los puertos hacia el contenedor, filtrando desde dónde se puede acceder:

    firewall-cmd --zone=server --add-source=12.34.56.78/32
    firewall-cmd --zone=server --add-forward-port=port=80:proto=tcp:toaddr=172.31.255.10

En este caso estamos usando una zona de servidor a la que sólo se puede acceder desde la IP 12.24.56.78 y que si lo hacemos por el puerto 80 será reenviado a la IP 172.31.255.10 de uno de los contenedores.

También podremos definir nosotros nuestra configuración: [https://docs.docker.com/compose/networking/](https://docs.docker.com/compose/networking/)

Podemos crear una nueva red:

```
networks:
  isard-network:
    name: isard-network
    driver: bridge
    ipam:
      config:
      - subnet: 172.31.255.0/24
        gateway: 172.31.255.1
```

Y en los contenedores fijar su ip si queremos:

```
services:
  isard-api:
    networks:
      isard-network:
        ipv4_address: 172.31.255.10
```

