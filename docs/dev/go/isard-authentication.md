# `isard-authentication`

Vamos a entender cómo funciona el servicio `isard-authentication`

## Qué hace?

- Autentica usuarios contra varios sistemas de autenticación:
    + Local (contra la base de datos de IsardVDI)
    + LDAP
    + SAML
    + Google (OAuth2)
- Sistema de registro por código
- Sistema de autoregistro (sólo LDAP)

## Cómo está el código organizado?

```sh
├── authentication
│   ├── authentication.go   # Definición de la interficie del servicio y implementación de este
│   ├── provider            # Paquete para los diferentes 'providers', es decir, diferentes sistemas de autenticación
│   │   ├── external.go     # Autenticación externa: se utiliza para las "external apps", cómo por ejemplo el plugin de Moodle
│   │   ├── form.go         # Autenticación a través del formulario del frontend
│   │   ├── google.go       # Autenticación con Google
│   │   ├── ldap.go         # Autenticación con LDAP
│   │   ├── local.go        # Autenticación con la DB local
│   │   ├── oauth2.go       # 'helper' para la autenticación por OAuth
│   │   ├── provider.go     # Definición de la interfície de provider
│   │   ├── saml.go         # Autenticación por SAML
│   │   └── testing.go      # 'helpers' para escribir unit tests
│   ├── testing.go          # 'helpers' para escribir unit tests
│   └── token.go            # Funciones relacionadas con la generación / validación de tokens JWT
├── build
│   └── package
│       ├── Dockerfile      # Paquetización del servicio
│       └── run.sh          # Entrypoint del servicio
├── cfg
│   └── cfg.go              # Configuración del servicio
├── cmd
│   └── authentication
│       └── main.go         # Paquete 'main'
├── model                   # Modelos de la DB
│   ├── group.go
│   ├── role.go
│   ├── secret.go
│   ├── user.go
│   └── user_test.go
└── transport
    └── http
        ├── helpers.go      # 'helpers' para el servidor HTTP
        ├── helpers_test.go # Tests de los 'helpers' para el servidor HTTP
        ├── http.go         # Servidor HTTP
        └── http_test.go    # Tests del servidor HTTP
```
