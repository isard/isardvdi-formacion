# `isard-websockify`

Vamos a entender cómo funciona el servicio `isard-websockify`

## Qué hace?

El servicio de `websockify` es el responsable de hacer funcionar los visores Web. El visor web, en realidad son 2 visores:  

- [noVNC](https://github.com/novnc/noVNC) -> Utiliza el protocolo `VNC`  
- [spice-web-client](https://github.com/isard-vdi/spice-web-client/) -> Utiliza el protocolo `Spice`  

El problema de estos dos visores es que, los protocolos que utilizan por debajo, utilizan `TCP` 'a pelo', y los navegadores no pueden crear conexiones `TCP`, sólo `HTTP` (que, a su vez, utiliza `TCP` por debajo):

```
| VNC  |  Spice  |  HTTP | <- El navegador sólo puede crear conexiones HTTP!
--------------------------
|           TCP          |
```

Cómo se resuelve? Con un servidor de *proxy*. Este servidor, se dedica a **traducir** de `Websocket` a `TCP` a secas.

`Websocket` es un protocolo encima de `HTTP`, que nos proporciona una conexión full duplex (lectura y escritura), y que todos los navegadores modernos entienden. Miremos el siguiente diagrama:

```mermaid
graph LR
    A[Navegador] <--> |Websocket| B(isard-websockify)
    B <--> |Consultar que el cliente tiene permisos| C(isard-api)
    B <--> |TCP| D(isard-hypervisor)
```

1. El cliente (navegador) se conecta a `isard-websockify`
2. `isard-websockify` comprueba que el cliente tenga permisos para conectarse al destino `(host + port)`
3. `isard-websockify` se conecta al destino y "relaciona" la conexión de `Websocket` con la de `TCP`
4. Cada byte que se escriba en cualquier dirección, en cualquier de las dos conexiones, se replicará a la otra


## Inspección del código