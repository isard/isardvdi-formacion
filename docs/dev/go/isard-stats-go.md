# `isard-stats-go`

Vamos a entender cómo funciona el servicio `isard-stats-go`

## Qué hace?

- Extrae estadísticas de uso de IsardVDI
- Las publica en formato Prometheus, para que el `isard-grafana-agent` las pueda recoger y guardar en el Prometheus
- Extrae estadísticas según dónde se ha arrancado

## Cómo está el código organizado?

```sh
├── build
│   └── package
│       ├── Dockerfile
│       └── run.sh
├── cfg
│   └── cfg.go
├── cmd
│   └── stats
│       └── main.go
├── collector                      # Paquete de los distintos recolectores de estadísticas
│   ├── collector.go               # Definición de la interficie de collector
│   ├── conntrack.go               # Collector conntrack. Sirve para las estadísticas de los visores RDP
│   ├── domain.go                  # Collector domain. Extrae estadísticas de las VM del libvirt
│   ├── domain_internal_test.go
│   ├── hypervisor.go              # Collector hypervisor. Extrae estadísticas del hypervisor
│   ├── isardvdi-api.go            # Collector API. Extrae estadísticas de la DB (número de usuarios, de desktops, etc)
│   ├── isardvdi-authentication.go # Collector authentication. Extra estadísticas de los inicios de sesión
│   ├── oci.go                     # Collector OCI. Extrae estadísticas de Oracle
│   ├── socket.go                  # Collector socket. Extrae estadísticas de los visores Spice y VNC
│   └── system.go                  # Collector system. Extrae estadísticas del sistema
└── transport
    └── http
        └── http.go
```
