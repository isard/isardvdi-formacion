# Python

El lenguaje python fué implementado allá por finales de la década de los '80. El autor principal, Van rossum, le puso el nombre del sow de la BBC *Monty Phython's Flying Circus*.

Se trata de un leguaje *compilado*, no a código máquina, sinó a un intermedio, [bytecode](https://en.wikipedia.org/wiki/Bytecode). Esto le proporciona mejor rendimiento que un leguaje interpretado, pero algo menor que uno compilado a código máquina.

La mayoría de distribuciones Linux llevan ya este lenguaje instalado. Si no podemos encontrar las órdenes para instalarlo o los instaladores en [https://www.python.org/](https://www.python.org/).

No somos muy fans del "Hello World" tradicional, que no aporta mucho. Vamos a introducirnos al Python con algunos ejemplos más prácticos.

Para realizar los ejemplos podemos usar diferentes métodos:

- **python3**: intérprete de comandos que viene con el Python
- **ipython3**: intérprete avanzado de comandos que podemos instalar con `apt install ipython3`
- **fichero**: podemos editar un fichero y ejecutarlo con el intérprete `python script.py`

## Hello World

El típico *Hello World!* no nos parece que sirva de mucho. Así que vamos con algo más interesante como bienvenida al python.

Imaginemos que queremos hacer un programa que cada día nos facilite tener un resumen de los eventos culturales que hay en Euskadi. Para eso hay una api [https://opendata.euskadi.eus/apis/-/apis-open-data/](https://opendata.euskadi.eus/apis/-/apis-open-data/) en dónde podemos consultar, y adaptar los resultados como nosotros queramos.

Primero iremos al navegador para analizar los datos devueltos en la API de cultura (formato JSON): [https://api.euskadi.eus/culture/events/v1.0/events?year=2023&month=5&day=25](https://api.euskadi.eus/culture/events/v1.0/events?year=2023&month=5&day=25)

Y los procesaremos en python:

```Python
#!/usr/bin/env python3
import requests

respuesta = requests.get("https://api.euskadi.eus/culture/events/v1.0/events?year=2023&month=5&day=25")

if respuesta.status_code == 200:
    print("Hemos contactado con la api de eventos!")
    print("Y hay "+str(respuesta.json()["totalItems"])+" eventos")

    i=1
    pagina1=len(respuesta.json()["items"])
    print("Estos eventos hay en la primera página")
    for evento in respuesta.json()["items"]:
        print(str(i)+"/"+str(pagina1)+": "+evento["nameEu"]+" / "+evento["municipalityEu"])
        i+=1
else:
    print("Pues no podemos consultar los eventos ahora mismo. Pruebe en unos minutos")
```

Aquí vemos que importamos una libreria de Python, `requests`, que nos permite hacer peticiones a una *URL*. En la respuesta tenemos un objeto con atributos, como por ejemplo el *status_code* de la respuesta HTTP.

Seguidamente pasamos a procesar los datos de la respuesta con el método `json` del objeto respuesta. Esto nos permite procesar el texto (en formato json) que nos ha devuelto la API a un diccionario (con claves y valores) que nos será más fácil trabajar. Por ejemplo estamos cogiendo de ese diccionario el valor de la clave *totalItems*, que son los eventos.

A continuación iteramos en un bucle por cada *evento* en la lista de eventos que tenemos en la clave *items*. Por cada evento imprimiremos el progreso, el nombre del evento y la localidad en la que se realiza.

Aquí vemos que en python es importante la tabulación. El parseado del intérprete espera tabulación en las estructuras, y también los **:** al final de las declaraciones (condicionales, funciones, clases, bucles...).


## Paquetería

Para instalar paquetes de librerías externas podremos hacerlo con el gestor de paquetes de la distribución (`apt install python3-flask` en Ubuntu/Debian) o bien mediante el gestor de paquetería [https://pypi.org/project/pip/](https://pypi.org/project/pip/), que nos permitirá tener más control de las versiones de paquetes instaladas.

```Bash
apt install python3-pip
pip3 install pythonping==1.0.15
```

Podemos buscar paquetes en [https://pypi.org/](https://pypi.org/), como el del ejemplo [https://pypi.org/project/pythonping/](https://pypi.org/project/pythonping/), que nos permitirá ejecutar `ping` desde dentro del código python.

## Estructura

En python, como en la mayoría de lenguajes, tenemos paquetes que importamos, funciones, clases...

### Libreria estándard

En python tenemos ya estas implementaciones en cuanto a:

- [funciones](https://docs.python.org/3/library/functions.html)
- [constantes](https://docs.python.org/3/library/constants.html)
- [tipos de datos](https://docs.python.org/3/library/stdtypes.html)
- [excepciones](https://docs.python.org/3/library/exceptions.html)
- [procesado de texto](https://docs.python.org/3/library/text.html)

Y muchas más [https://docs.python.org/3/library/index.html](https://docs.python.org/3/library/index.html) que iremos viendo con ejemplos.


### Importar paquetes

```Python
#!/usr/bin/env python3
import time
from pythonping import ping
from .decorators import is_admin

time.sleep(1)
ping("8.8.8.8")
print("Is an admin? "+str(is_admin("usuario")))
```

En el primero estamos importando todo el módulo `time` de python, que tendrá sus funciones (sleep por ejemplo). Como solo estamos usando la función `sleep`, también podríamos haber hecho:

```Python
from time import sleep

sleep(1)
```

En el segundo estamos importando solo la función `ping` del módulo `pythonping`.

En el tercero estamos importando del fichero `decorators.py` que tenemos en la misma carpeta, la función `is_admin`

### Listas y diccionarios

En python hay muchas funciones para trabajar con listas y con diccionarios.

```Python
#!/usr/bin/env python3
frutas = ["manzana","plátano","cereza"]

print("Hay cereza" if "cereza" in frutas else "No hay cereza")
print(f"Tenemos {len(frutas)} frutas")
print(f"Las dos primeras frutas son {frutas[:2]}")
frutas.remove("plátano")
print(f"Ahora solo queda {' '.join(frutas)}")
frutas.append("mandarina")
print(f"Pero ahora también tenemos {frutas[-1]} entre {' '.join(frutas)}")
```

Y también con los diccionarios.

```Python
#!/usr/bin/env python3
comida = {
    "frutas": ["manzana", "plátano", "cereza"],
    "verduras": ["lechuga", "tomate", "cebolla", "pimiento"],
    "carnes": ["pollo", "vaca", "cerdo", "pavo", "jabalí"],
}

print(
    "Hay carnes de pollo"
    if "carnes" in comida.keys() and "pollo" in comida["carnes"]
    else "Se nos ha acabado el pollo"
)
from random import randrange
print(f"Os recomiendo comer {comida['verduras'][randrange(len(comida['verduras']))]}")
```

### Listas de comprensión

Las listas de comprensión nos permiten simplificar (`pythonic way`) código, especialmente en iteraciones. Aquí tenemos un bucle que podemos cambiar por lista de comprensión.

```Python
#!/usr/bin/env python3
comida = {
    "frutas": ["manzana", "plátano", "cereza"],
    "verduras": ["lechuga", "tomate", "cebolla", "pimiento"],
    "carnes": ["pollo", "vaca", "cerdo", "pavo", "jabalí"],
}

total=0
for key,value in comida.items():
    total+=len(value)
print(f"Tenemos {total} alimentos")

total = sum(len(value) for key, value in comida.items())
total_vegano = sum(len(value) for key, value in comida.items() if key != "carnes")
print(f"Seguimos teniendo {total} alimentos, de los cuales {total_vegano} son aptos para veganos")
```

### Funciones

Las funciones podrán tener parámetros obligatorios y opcionales. La declaración *global* dentro de una función nos permitirá acceder y modificar variables fuera de las funciones.

```Python
#!/usr/bin/env python3
variable_global = 2
def funcion(parametro, parametro_opcional=1):
    global variable_global
    variable_local = 3
    return parametro * variable_global * variable_local * parametro_opcional

print(funcion(4))
```

Las funciones implementadas en la libreria de Python son [https://docs.python.org/3/library/functions.html](https://docs.python.org/3/library/functions.html)

### Clases

Una clase nos permitirá implementar programación orientada a objetos, asignado propiedades, funciones, herencias... a una instancia que crearemos desde la clase.

```
#!/usr/bin/env python3
class Usuario:
    def __init__(self, nombre_usuario)
        self.nombre=nombre_usuario
    
    def get_nombre_usuario_mayusculas():
        return self.nombre.toupper()

usuario1 = Usuario("Miguel")
print(usuario1.get_nombre_usuario_mayusculas()
```

- [https://docs.python.org/3/tutorial/classes.html](https://docs.python.org/3/tutorial/classes.html)