# Logs en IsardVDI

Aunque realizaremos un análisis más profundo en el apartado de *troubleshooting*, vamos a repasar como ver los logs de contenedores.

```
docker logs isard-api
```

Con este comando veremos los logs desde que se creó el container hasta el momento actual. Durante su vida puede haberse reiniciado (stop/run) varias veces, y todos esos logs estarán, hasta que eliminemos definitivamente el contenedor ```docker rm isard-api```

Algunas opciones útiles son:

- **-f**: para ver los logs en tiempo real
- **--since 1m**: para solo ver el histórico del último minuto.
- **&> fichero**: para redirigir la salida a un *fichero* y procesarlo mejor.

Con el *docker compose* podemos ver también los logs del conjunto de contenedores definidos en el *docker-compose.yml* simultáneamente, intercalados:

```
docker compose logs
```

Tenemos el equivalente en opciones aquí [https://docs.docker.com/engine/reference/commandline/compose_logs/](https://docs.docker.com/engine/reference/commandline/compose_logs/).

Recordemos que és necesario en producción limitar la cantidad de logs que serán guardados:

```
version: '3.5'
services:
  isard-api:
    container_name: isard-api
    image: isard-api:latest
    logging:
      driver: "json-file"
      options:
        max-size: "100m"
        tag: "{{.ImageName}}|{{.Name}}|{{.ImageFullID}}|{{.FullID}}"
...
```

También podemos usar diferentes *drivers* del propio *docker compose* para reenviarlos a nuestra consola de monitorización [https://docs.docker.com/config/containers/logging/configure/#supported-logging-drivers](https://docs.docker.com/config/containers/logging/configure/#supported-logging-drivers).

En IsardVDI hemos optado por usar el contenedor [Grafana Agent](https://grafana.com/docs/agent/latest/set-up/install-agent-docker/) de recogida y envio de logs ya que en el *stack* de tecnologías optamos por *Grafana* + *Prometheus*.