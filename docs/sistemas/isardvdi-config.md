# docker en IsardVDI

IsardVDI tiene diferentes servicios y cada uno de ellos tiene su *Dockerfile* y quizá ficheros también que se añaden al hacer el *build*. Revisemos por ejemplo el del contenedor python/flask *isard-api*:

- [https://gitlab.com/isard/isardvdi/-/blob/main/api/docker/Dockerfile](https://gitlab.com/isard/isardvdi/-/blob/main/api/docker/Dockerfile)

Los ficheros *compose* de tipo *yml* se encuentran separados por servicios y por el tipo de uso en el directorio [https://gitlab.com/isard/isardvdi/-/tree/main/docker-compose-parts](https://gitlab.com/isard/isardvdi/-/tree/main/docker-compose-parts). Abrimos por ejemplo el de *isard-api* [https://gitlab.com/isard/isardvdi/-/blob/main/docker-compose-parts/api.yml](https://gitlab.com/isard/isardvdi/-/blob/main/docker-compose-parts/api.yml).

Vemos que todos los servicios tienen variantes según el uso que se les va a dar. Alguien debe decidir qué ficheros escojer y como encadenarlos para formar el *docker-compose.yml* final, junto con las variables personalizadas de nuestra instalación:

- **isardvdi.cfg**: Es el fichero de personalización de variables.
- **build.sh**: Es el *script* que generará el *docker-compose.yml* deseado a partir de las variables en *isardvdi.cfg*

Revisemos el fichero, autocomentado, *isardvdi.cfg.example*: [https://gitlab.com/isard/isardvdi/-/blob/main/isardvdi.cfg.example](https://gitlab.com/isard/isardvdi/-/blob/main/isardvdi.cfg.example)

Por regla general, las nuevas variables si están comentadas, cogerán el valor que tengan en este fichero por defecto.

