## Sistemas

### Distribución de software (4h)

Partimos de una evolución del software que ha comportado aplicaciones que mezclan diferentes lenguajes y tecnologías, a la vez que hacen uso de numerosas librerias. Cada uno con sus propios gestores de paquetes. Esto hizo que el CI/CD (Continuous Integration / Continuous Deployment) se hiciera cada vez más problemático, ya que mantener el mismo ecosistema en desarrollo y en producción implicaba complejos problemas de incompatibilidad de versiones de librerias y paquetes.

Esta problemàtica llevó a la aparición de numerosos sistemas de empaquetado de las aplicaciones, siendo Docker uno de los más populares en la última década, pero no el único:

- Snap
- Flatpak
- CoreOS
- LXC

Estos sistemas *ligeros* de empaquetado de aplicaciones llevaron también a la era de los *microservicios* y de las *api* para la comunicación entre contenedores, cada uno ejecutando una función simple, clara y bien definida ("haz solo una tarea, pero hazla bien").

Lectura recomendada: [https://avatao.com/blog-life-before-docker-and-beyond/](https://avatao.com/blog-life-before-docker-and-beyond/)

#### Docker (1:15h)

Docker nació como el primer entorno de usuario que hace uso del conjunto de *Kernel namespaces* y de los *cgroups* que permiten aislar los procesos y recursos dentro de un 'contenedor' y que, mediante las jaulas *chroot* de linux se ejecutan en un directorio aislado también del resto del sistema.

Lectura recomendada: [https://medium.com/@BeNitinAgarwal/understanding-the-docker-internals-7ccb052ce9fe](https://medium.com/@BeNitinAgarwal/understanding-the-docker-internals-7ccb052ce9fe)


### Orquestradores (0:45h) 

Para poder arrancar los diferentes *microservicios* en contenedores y poder definir sus variables de entorno, su configuración de red, sus directorios... se hacía engorroso ejecutar y definir cada uno de ellos en la línea de órdenes. Por eso se hizo necesaria la aparición de sistemas para definir infraestructuras de contenedores.

- **docker compose**: permite definir cómo se iniciaran diferentes contenedores y las relaciones entre ellos, en un único servidor.
- **docker swarm**: nos permitirá hacer lo mismo pero de manera distribuida entre diferentes servidores.
- **Kubernetes**: permitirá un mayor nivel de complejidad en la definición del escalado de los servicios según la demanda.

Lectura recomendada: [https://www.techrepublic.com/article/simplifying-the-mystery-when-to-use-docker-docker-compose-and-kubernetes/](https://www.techrepublic.com/article/simplifying-the-mystery-when-to-use-docker-docker-compose-and-kubernetes/)

#### Docker compose

En IsardVDI usamos *docker compose*. Migrar un sistema definido con *docker compose* a cualquier otro sistema de orquestración no debería ser complejo, ya que todos parten de la base de funcionamiento de *docker*.

Lectura recomendada: [https://docs.docker.com/compose/features-uses/](https://docs.docker.com/compose/features-uses/)