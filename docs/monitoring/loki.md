# Loki

![](https://i.imgur.com/6WmcYyo.png)

**Grafana Agent** se encarga de recolectar métricas y registros de diferentes fuentes, como sistemas operativos, servicios y aplicaciones. Los datos en este caso los envía a **Loki**, Loki recibe los registros de Grafana Agent y los organiza en un formato optimizado para su consulta y visualización y **Grafana** se conecta a Loki para consultar y mostrar los registros almacenados.

![](https://i.imgur.com/wLapudJ.png)

!!! Learn More "Referencia"
    <https://grafana.com/oss/loki/>

!!! Info "Documentación"
    <https://grafana.com/docs/loki/latest/fundamentals/overview/>


### Qué es?
**Grafana Loki** es una herramienta que se usa para recopilar y visualizar los registros de aplicaciones y sistemas en tiempo real. En lugar de almacenar los registros en un solo lugar, los distribuye en múltiples nodos para mejorar la escalabilidad y la redundancia. Esto es especialmente útil en entornos de microservicios y contenedores, donde el volumen de registros puede ser muy grande y la infraestructura puede ser muy dinámica.

### Queries con Loki

!!! Info "Documentación"
    <https://grafana.com/docs/loki/latest/logql/>

En Loki, las consultas se basan en lenguaje de consulta LogQL. Puedes ingresar tu consulta el `Explorer` de Loki en Grafana.

![](https://i.imgur.com/yQb2M9b.png)

- Para crear la consulta hay que seleccionar las etiquetas que se deseen en ![](https://i.imgur.com/NJaHyIC.png) o al cambiar al modo constructor ![](https://i.imgur.com/fHoI7kO.png) que puede ser de ayuda al empezar a crear Queries.

- Además, se pueden ver diferentes ejemplos en ![](https://i.imgur.com/MGDbwax.png), donde te muestran ejemplos tanto de formato log o metricas:

    ![](https://i.imgur.com/KeO0PFA.png)

**Elementos clave del formato de consulta en Loki:**

**Etiquetas**: Las consultas en Loki se basan en etiquetas que se agregan a los registros de los logs. Estas etiquetas permiten filtrar y agrupar los registros. Puedes usar etiquetas(`Label`) por ejemplo, como `app` o `level` para especificar condiciones de búsqueda más específicas.

`{app="myapp", level="error"}`

**Funciones**: Loki proporciona diversas funciones para manipular y analizar los registros. Por ejemplo, puedes utilizar la función `filter` para filtrar registros basados en condiciones específicas, o la función `sum` para realizar agregaciones.

`{app="myapp"} | filter(level = "error")`

**Rangos de tiempo**: Puedes especificar un rango de tiempo para tu consulta. Puedes utilizar expresiones como `5m` (5 minutos) o `1h` (1 hora) para definir la ventana de tiempo en la que deseas buscar los registros.

`{app="myapp"} | filter(level = "error") | 5m`

**Operadores lógicos**: Puedes utilizar operadores lógicos como `and` y `or` para combinar condiciones en tus consultas.

`{app="myapp"} | filter(level = "error" and region = "us-west")`


!!! Example "Ejemplo 1"
    ***Se puede utilizar Loki para comprobar los logs de diferentes contenedores y revisar cuando haya algún problema de Isard como cuando no arranca correctamente una máquina y el mensaje de Error no parece ayudar, ver el procedimiento que ha seguido un desktop en concreto:***

    - En la vista de administrador se puede extraer el identificador (Id) del escritorio, con `Ctr+Alt+i`, esto es de mucha ayuda al querer ver los logs solamente de ese escritorio:

    ![](https://i.imgur.com/FD5T8Ki.png)
    
    -  En el apartado `Explorer` de Grafana, se escoge el contenedor isard-engine que es donde se puede ver la actividad del escritorio: 
    ```
    {container_name="isard-engine"} |= "_local-default-admin-admin_downloaded_slax93"
    ```
    - En esta consulta extraemos de la etiqueta `container_name` el contenedor `isard-engine` y luego al añadir `|_=""` escogemos el id del escritorio de entre todos los logs:
    
    ![](https://i.imgur.com/jiAZXpC.png)
    - Y así solo se verá el registro de actividad del escritorio:
    ![](https://i.imgur.com/oSL8SZF.png)

!!! Example "Ejemplo 2"
    ***Otro ejemplo que es muy útil es poder ver los logs de distintos contenedores simultáneamente en una hora en concreto***
    
    - Se puede hacer con el apartado ![](https://i.imgur.com/gb7lnOH.png), con esto la pantalla se divide y podemos elegir dos contenedores para ver al mismo tiempo:
    
    ![](https://i.imgur.com/lphf5mJ.png)
    
    - Se selecciona el rango de tiempo en el que se quiera ver los logs de los dos contenedores o si se desea el mismo rango de tiempo para los dos, hay que hacer clic en ![](https://i.imgur.com/U3Btp8E.png), con esto se sincronizará con la otra pantalla y filtrará por el rango de tiempo que elijamos, rango de tiempo absoluto es decir hace 1h, 5m... o el rango de tiempo más especifico que se puede añadir hasta el día:
    
    ![](https://i.imgur.com/m2AzMWL.png)
    