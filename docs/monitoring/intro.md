# Estructura

## Monitorización

![](https://i.imgur.com/APuVXa2.png)

!!! Learn More "Referencia"
    <https://grafana.com/blog/2021/10/28/open-source-done-right-why-canonical-adopted-grafana-loki-and-grafana-agent-for-their-new-stack/>

Dentro de IsardVDI tenemos un listado de contenedores, los cuales tienen como función recopilar información para poder mostrarla en la parte de monitorización del [**Grafana**](https://grafana.com/docs/grafana/latest/fundamentals/).

De estos contenedores hay 4 que no son nuestros:

- [isard-grafana](https://grafana.com/docs/grafana/latest/introduction/) 
- [isard-grafana-agent](https://grafana.com/docs/agent/latest/)
- [isard-prometheus](https://prometheus.io/docs/introduction/overview/) 
- [isard-loki](https://grafana.com/docs/loki/latest/fundamentals/overview/) 

**Grafana**: Es una plataforma de visualización y monitorización que permite crear paneles y gráficos interactivos basados en los datos recopilados por Prometheus y otras fuentes. Grafana puede conectarse a Prometheus para consultar y mostrar métricas.

**Grafana-Agent**: Es un componente opcional que se puede utilizar para recopilar y enviar métricas y logs a Prometheus y Loki, respectivamente. Grafana-Agent actúa como un intermediario entre las aplicaciones y los sistemas de monitorización, recopilando los datos y enviándolos a los sistemas correspondientes.

**Prometheus**: Es un sistema de monitorización y series de tiempo que recopila métricas y datos de estado de diversas fuentes. Grafana se comunica directamente con Prometheus para obtener los datos necesarios para mostrar en los paneles y gráficos.

**Loki**: Es un sistema de registro de eventos y logs que permite almacenar y consultar logs de forma eficiente. Grafana se integra con Loki para mostrar registros de eventos en los paneles y permitir la exploración de logs.

Hay 3 contenedores más que utilizamos para monitorizar:

- isard-stats-rethinkdb
- [isard-stats-cadvisor](https://github.com/google/cadvisor)
- [isard-stats-node-exporter](https://github.com/prometheus/node_exporter)

**isard-stats-rethinkdb**: Es el contenedor creado para encargarse de conectarse y recopilar los logs de la BBDD de RethinkDB, nos ayuda a poder ver el número de conexiones a la BBDD, las consultas por segundo...

![](https://i.imgur.com/NQGuSBX.png)
    
**isard-stats-cadvisor**: Este contenedor se utiliza para recopilar el consumo y carga de los contenedores, es de ayuda para poder averiguar si un contenedor está consumiendo demasiado, se comunica con prometheus.

![](https://i.imgur.com/gSqTyv8.png)

**isard-stats-node-exporter**: Gracias a este contenedor podemos sacar información mucho más concreta, como por ejemplo el estado de la red, CPU, RAM de manera más extensa, también se comunica con prometheus para poder extraer las métricas.

![](https://i.imgur.com/tP7EC2K.png)

En un servidor único se monta con todos los contenedores de monitorización:

**ALL-IN-ONE**

- isard-grafana
- isard-grafana-agent
- isard-stats-go
- isard-loki
- isard-stats-rethinkdb
- isard-prometheus
- isard-stats-cadvisor
- isard-stats-node-exporter

En cambio, cuando es un clúster se separa por cada nodo, en este caso web sería donde está arrancado el Isard y tendría estos contenedores:

**CLÚSTER (WEB)**

- isard-grafana
- isard-grafana-agent
- isard-stats-go
- isard-loki
- isard-stats-cadvisor
- isard-prometheus
- isard-stats-node-exporter
- isard-stats-rethinkdb

En el caso de los nodos que son hypervisores, solamente se arrancan estos contenedores:

**CLÚSTER (HYPERVISOR)**

- isard-grafana-agent
- isard-stats-go
- isard-stats-cadvisor
- isard-stats-node-exporter

```mermaid
flowchart LR
    subgraph Web
        A[Grafana Agent] -->|Extract stats using HTTP endpoint| B(isard-stats-go)
        A --> |Extract stats using HTTP endpoint| C(isard-cadvisor)
        A --> |Extract stats using HTTP endpoint| D(isard-node-exporter)
        A --> |Extract stats using HTTP endpoint| E(isard-stats-rethinkdb)
    end

    subgraph Monitor
        X{Grafana} --> |Query Prometheus for stats|Z
        X --> |Query Loki for logs|ZZ
        mA[Grafana Agent] -->|Extract stats using HTTP endpoint| mB(isard-stats-go)
        mA --> |Extract stats using HTTP endpoint| mC(isard-cadvisor)
        mA --> |Extract stats using HTTP endpoint| mD(isard-node-exporter)
        mA -...-> |Send stats to Prometheus| Z
        mA -...-> |Send logs to Loki| ZZ
    end

    subgraph Hypervisor
        hA[Grafana Agent] -->|Extract stats using HTTP endpoint| hB(isard-stats-go)
        hA --> |Extract stats using HTTP endpoint| hC(isard-cadvisor)
        hA --> |Extract stats using HTTP endpoint| hD(isard-node-exporter)
    end

    A -...-> |Send stats to Prometheus| Z[(Prometheus)]
    hA -...-> |Send stats to Prometheus| Z

    A -...-> |Send logs to Loki| ZZ[(Loki)]
    hA -...-> |Send logs to Loki| ZZ[(Loki)]
```