# Ejercicio: Crear máquinas virtuales

## Introducción

Las máquinas virtuales simularán un hardware que nos permitirá instalar sistemas operativos invitados (*guest).

En este ejercicio se crearán y gestionarán tanto desde interfaz gráfica (*virt-manager*) como des de línea de comandos. También se usará virtualización nativa *qemu*.

## Preparación

Haremos uso del *virt-manager* para crear y gestionar hardware de máquinas virtuales con el *hipervisor de código abierto KVM/qemu*:

Requisitos:

- **CentOs/*Fedora**: *dnf install @Virtualization -y
- **Debian/*Ubuntu**: *apt install qemu-kvm libvirt-clientes qemu-utils libvirt-daemon-system virt-viewer virt-manager -y*

Comprobáis que el servicio *libvirtd está funcionando: *systemctl status libvirtd* y si no lo ponéis en marcha.

Comprobad que tenéis instalada la aplicación *virt-manager*.

## Realización

1. **Crea una máquina con el *virt-manager* que nos permita instalar *DSL Linux* a partir del servidor de la ISO descargada del web. Inícia-la. Usa un disco duro virtual *qcow2 incremental creado por ti de 1GB.**
    
    Para crear el disco:

    ```qemu-img create -f qcow2 dsl.qcow2 1G```

    Podéis comprobar que el disco creado sólo ocupa unos 200K (es incremental y aún no le hemos añadido nada).

    DSL Linux: [http://distro.ibiblio.org/damnsmall/current/current.iso](http://distro.ibiblio.org/damnsmall/current/current.iso)

    Crearemos una nueva máquina virtual y seleccionaremos la instalación desde la ISO descargada.

    Una vez arrancada podremos instalarla en el disco clicando con el botón derecho en el menú contextual *Apps ->Tools->Install to Hard Drive*

    Una vez instalado podemos parar la máquina, cambiar su arranque (*boot*) a disco duro y comprobar que se ha instalado. Podemos quitar también la ISO. Ahora si miramos el tamaño del fichero *dsl.qcow2* veremos que se ha incrementado proporcionalmente.

    A veces, y según el sistema operativo que estemos instalando, hay que jugar con los controladores de cada dispositivo emulado. Los controladores *virtio* suelen venir ya en las distribuciones Linux, pero no en Windows. Si no se detecta algún dispositivo, cambiar a un dispositivo no virtual (emulado del real).

    Los sistemas operativos Linux también suelen permitir el cambio de algun hardware en caliente, como las tarjetas de red.


2. **Con la máquina arrancada hacéis uso de la línea de órdenes para interactuar con la máquina virtual:**

    - Listar todas las máquinas virtuales que tenemos definidas: ```virsh list```. ```virsh list --all``` para ver también las paradas.
    - Suspender la máquina virtual (*pausar*): ```virsh suspend mivm```
    - Resumir una máquina virtual pausada (continuar ejecutando): ```virsh resume mivm```
    - Enviar señal de apagado a la máquina virtual: ```virsh shutdown mivm```
    - Forzar la parada de la máquina virtual: ```virsh destroy mivm```
    - Editar la configuración XML de la máquina virtual: ```virsh edit mivm```
    - Iniciar la máquina virtual: ```virsh start mivm```
    - Ver la *URI de *conexió al visor de la máquina virtual: ```virsh domdisplay mivm``` ```virsh vncdisplay mivm```
    - Guardar la configuración XML de hardware de la máquina virtual en un fichero: ```virsh dumpxml mivm > mivm.xml```
    - Eliminar la máquina virtual: ```virsh undefine mivm```. (Nota: no se borra el disco ni cd/dvd)
    - Crear de nuevo la máquina virtual a partir del fichero de hardware: ```virsh define mivm.xml```

3. **Con la utilidad *virt-install* podemos crear máquinas virtuales a partir de repositorios que hay en Internet o también a partir de ISO de instalación de sistemas operativos. Lo tendréis que instalar y crear un disco primero donde instalar el sistema operativo que usaremos (*Ubuntu 22*)**:

    Creamos un disco con *qemu-img*: *qemu-img create -f qcow2 ubuntu22.*qcow2 4G

    Y hacemos una instalación desatendida dentro:

    ```
    virt-install \
    --name ubuntu2204 \
    --ram 2096 \
    --disk path=ubuntu2204.img,size=4 \
    --vcpus 2 \
    --os-variant ubuntu22.04 \
    --network bridge=br0 \
    --graphics none \
    --console pty,target_type=serial \
    --location /home/ubuntu-22.04-live-server-amd64.iso,kernel=casper/vmlinuz,initrd=casper/initrd \
    --extra-args 'console=ttyS0,115200n8' 
    ```


8. **Descargáis del web http://www.qemu-advent-calendar.org/2016/ el día 6 donde hay el *MenuetOs (también lo tenéis en este directorio). Mirando el fichero run.sh realizáis:**

    Para arrancar esta máquina virtual deberemos mirar en el contenido de fichero. Allí encontraremos un *README* con instrucciones de cómo arrancarla con *qemu* directamente y un fichero de disco (puede ser qcow2, img, raw).

    Podemos usar el *virt-manager* para crear una nueva máquina virtual a partir de ese disco y arrancarla.

    A veces las imágenes de disco de esa web son tan pequeñas que son en extensión *fd* y hay que añadirlas como *disquette* y arrancar con él.


9. **Creáis ahora esta misma máquina virtual *MenuetOS* mediante el entorno gráfico del *virt-manager* y arrancadla** 

    - 1/4 Habéis tenido que realizar algún cambio en el hardware por defecto para que pudiera arrancar?

    - 2/4 Como queremos instalar el sistema operativo?

        Hay que indicar que usaremos nuestro propio disco.

    - 3/4 Qué tipo de sistema operativo y versión has seleccionado?

        Podemos usar cualquier plantilla de sistema operativo base, pero al tratarse de un sistema operativo muy viejo, mejor usar alguna antigua.

    - 4/4 Cuánta memoria e hilos de procesador has seleccionado?

        Este ejemplo consume poca memoria, así que podemos poner cualquier valor. Para la CPU, difícilmente este sistema operativo estaría diseñado como multiprocesador o multithreading, con lo cual con una será suficiente.


10. **Si tenéis una ISO de Windows ahora una máquina virtual Windows que use para la interfaz de red y de disco duro los controladores *virtio.**

    - Imágen descargable: [https://www.microsoft.com/es-es/software-download/windows10ISO](https://www.microsoft.com/es-es/software-download/windows10ISO)
    - Controladores virtuales: [https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.229-1/](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.229-1/)


    Podéis seguir la guia de instalación: [https://isard.gitlab.io/isardvdi-docs/install/win10_install_guide.es/](https://isard.gitlab.io/isardvdi-docs/install/win10_install_guide.es/)